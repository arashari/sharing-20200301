% Intro to vue.js 2
% Tetamba Studio
% March, 2020

# Review Exercise

- belum implement vue/component
  - v-sesuatu itu buat apa?
  - nama variable?
  - penggunaan component
  - ...
- kendala:
    - mau ngubah data konten-nya
    - halo, selamat datang _undefined_
    - vue nya ngga jalan
    - ...
- vue-cli
- bootstrap-vue
- ...

# Component

- data
- props
- methods
- computed
- watchers

# List of Object

```js
const a = [1, 2, 3, 4, 5]
const b = [
  { value: 1 }, 
  { value: 2 },
  { value: 3 },
  { value: 4 },
  { value: 5 }
]
```

# Conditional render

- v-if
- v-else
- v-else-if

# Component Life Cycle

- created
- mounted
- updated
- destroyed

# Event

komunikasi antar component

# References

- [https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)
- [https://medium.com/badr-interactive/mengenal-lifecycle-hooks-pada-vue-js-78cd2225a69](https://medium.com/badr-interactive/mengenal-lifecycle-hooks-pada-vue-js-78cd2225a69)

# Homework Ashari

- benerin contoh event emitter dari child