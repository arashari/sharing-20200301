% Introduction
% Tetamba Studio
% March 2020

# Goal

"real" programming di dunia kerja

_\*at least di sini_

# Disclaimer

- kebanyakan dari pengalaman pribadi
- "sotoy" first
- jangan percaya 100%
- tidak bertanggung jawab atas kerugian apapun

---

> mari kita belajar bareng ;)

# Prerequisite

- internal
    - curiosity
    - active
    - ...

- external
    - english
    - logic
    - basic programming
    - ...

# Ask Yourself

::: columns
:::: column
- branching
    - if
    - switch case
- looping
    - for
    - while
    - until
- data type
    - array
    - stack
    - map
    - queue
    - tree
- rekursif
::::
:::: column
- paradigms
    - object oriented
    - functional
    - declarative
- sorting
    - bubble sort
    - merge sort
    - quick sort
- shortest path
    - dijkstra's algorithm
    - A\*
    - BFS
    - DFS
- ...
::::
:::


# Conclusion 1

- learn the basic
- "what i need for now"
- don't reinvent the same wheel

# Warming Up

- belajar berhitung 1 sampai 12
- banyak orang
- ganti-gantian
- N adalah jumlah orang

---

- N = 4
  
  > 1 2 3 4

- N = 12
  
  > 1 2 3 4 5 6 7 8 9 10 11 12

- N = 15
  
  > 1 2 3 4 5 6 7 8 9 10 11 12 1 2 3

- N = 100

  > ?

# Answer

```php
$n = 100;
$count = 1;

for ($i = 0; $i < $n; $i++) {
  echo $count . " ";
  $count++;

  if ($count > 12) {
    $count = 1;
  }
}
```

# Answer

```php
$n = 100;

while ($n > 0) {
  for ($i = 0; $i < 12; $i++) {
    echo ($i + 1) . " ";
    $n--;

    if ($n < 1) {
      break;
    }
  }
}
```

# Answer

```php
$n = 100;

for ($i = 0; $i < $n; $i++) {
  echo (($i % 12) + 1) . " ";
}
```

# Conclusion 2

- as long it works, its (probably) fine ;)
- constraints:
    - time
    - memory
    - storage

---
DUNIA KERJA

# New Challenges

- client first
- business process
- timeline
- changes
- real user
- ...

# Prerequisite 2

- hubungan dengan orang lain
    - belajar memahami orang
    - bekerja dalam tim
    - ...

- diri sendiri
    - self management
    - disiplin
    - menghargai waktu
    - terus belajar
    - update dengan perkembangan
    - debugging skill
    - googling skill
    - ...

<!-- # Ask Yourself 2

- CRUD
- login/logout
- export/import
- API
- ...

# Ask Yourself 2

- migration
- continuity
- scaling up
- optimization
- concurrency
- consistency
- ...

# IT Adaptation

1. digitalization
2. optimization
3. disruption -->

# Basic Knowledge

- project life cycle
- development workflow
- common tools

# Project Life Cycle

1. planning
      - requirement gathering
      - analysis
      - design
      - ...
2. development
      - coding
      - testing
      - deployment
      - ...
3. closing
      - user acceptance test
      - migration
      - ...

# Project Methodologies

- waterfall
- agile
    - scrum
- ...

# 3 Pillars of Agile

- fast
- flexible
- enduring

# Development Workflow

```java
while (true) {
  plan();
  code();
  test();
}
```

# Development Workflow

- sprint planning
- development + testing
    - daily meeting/standup
- sprint review

# Common Tools

::: columns
:::: column
- vcs
    - git
- code repository
    - gitlab
- framework/library
    - yii2
    - laravel
    - react
    - react native
    - spring boot
    - android/ios native
::::
:::: column
- design/mockup/wireframe
    - figma
    - balsamiq
    - draw.io
    - invision
    - adobe XD
- support system
    - trello
    - gdoc
    - whatsapp
- ...
::::
:::

# Conclusion 3

> "Semangat." \- Ashari, 2020

# References

- [https://3pillarsofagile.github.io/](https://3pillarsofagile.github.io/)