% Intro to vue.js
% Tetamba Studio
% March, 2020

# What

- javascript frontend framework
- client side
- declarative

# Why

- reactivity

# Installation

- embed
- standalone

# Directives

- v-if
- v-bind
- v-on
- v-model
- v-for

# Composability

- component

# Data

- data
- props
- computed
- watchers

# Exercise

- buat halaman login + jumbotron
- combine dengan css bootstrap
- at least 1 component

# Homework

- install npm menggunakan nvm