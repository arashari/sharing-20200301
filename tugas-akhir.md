---
geometry: margin=2cm
---

# tugas akhir

membuat sebuah website blog

## spesifikasi

*) opsional

### halaman admin
- digunakan untuk melakukan CRUD postingan
- digunakan untuk melakukan CRUD kategori
- digunakan untuk melakukan RD komentar
- untuk tiap halaman index, diberikan search
- di halaman index postingan, diberikan filter kategori
- *) hanya dapat diakses jika sudah login sebagai admin
- *) diberikan pagination untuk halaman index, maksimal 20 item per halaman

### halaman depan
- menampilkan daftar postingan diurutkan berdasarkan yang paling baru
- pada tampilan depan, hanya tampilkan judul dan 50 karakter awal dari konten (jika lebih, ganti menjadi "...")
- dapat mencari postingan berdasarkan judul ataupun konten
- dapat menampilkan postingan berdasarkan kategori tertentu
- dapat melihat detil dari suatu postingan
- dapat memberikan komentar pada suatu postingan
- *) daftar postingan dibuat pagination, maksimal 5 per halaman

### umum
- tidak boleh menggunakan css library bootstrap
- pembuatan api dibebaskan (grpc/restful/dsb) (laravel/json-server/spring boot/dsb)

### entitas
- user (admin)
  - username
  - password
- post
  - title
  - content
  - created_date
  - category_id
- category
  - title
- comment
  - content
  - post_id
