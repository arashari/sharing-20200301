import Axios from 'axios'

function get(query) {
  return Axios.get('http://localhost:3001/posts', {
    params: {
      q: query
    }
  })
}

export default {
  get
}
